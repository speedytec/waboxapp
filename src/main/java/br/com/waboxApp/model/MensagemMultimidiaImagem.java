package br.com.waboxApp.model;

import javax.persistence.Entity;

@Entity
public class MensagemMultimidiaImagem extends Mensagem {

	private String custom_uid;
	private String url;
	private String caption;
	private String description;
	private String url_thumb;
	
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl_thumb() {
		return url_thumb;
	}

	public void setUrl_thumb(String url_thumb) {
		this.url_thumb = url_thumb;
	}

	

	public String getCustom_uid() {
		return custom_uid;
	}

	public void setCustom_uid(String custom_uid) {
		this.custom_uid = custom_uid;
	}


	
	
}
