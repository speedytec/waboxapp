package br.com.waboxApp.model;
import com.fasterxml.jackson.annotation.JsonProperty;


public class MensagemResposta {
	
	@JsonProperty("custom_uid")
	private String custom_uid;
	
	
	@JsonProperty("success")
	private String success;

	@JsonProperty("error")
	private String error;

	
	public String getError() {
		return error;
	}


	public void setError(String error) {
		this.error = error;
	}


	public String getCustom_uid() {
		return custom_uid;
	}


	public void setCustom_uid(String custom_uid) {
		this.custom_uid = custom_uid;
	}


	public String getSuccess() {
		return success;
	}


	public void setSuccess(String success) {
		this.success = success;
	}


	@Override
	public String toString() {
		return "MensagemResposta [custom_uid=" + custom_uid + ", success=" + success + "]";
	}
	


}
