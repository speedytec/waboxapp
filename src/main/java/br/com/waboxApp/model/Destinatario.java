package br.com.waboxApp.model;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;

@Entity
public class Destinatario extends Pessoa{

	@Override
	public String toString() {
		return "Destinatario [getCidade()=" + getCidade() + ", getEndereco()=" + getEndereco()
				+ ", getIdentificador2()=" + getIdentificador2() + ", getIdentificador3()=" + getIdentificador3()
				+ ", getIdentificador1()=" + getIdentificador1() + ", getEmail()=" + getEmail() + ", getId()=" + getId()
				+ ", getNome()=" + getNome() + ", getNumero()=" + getNumero() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

	
	
	
	}
