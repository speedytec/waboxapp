package br.com.waboxApp.model;

import javax.persistence.Entity;

@Entity
public class MensagemTexto extends Mensagem {


	
	private String text;
	
	private String custom_uid;

	public String getCustom_uid() {
		return custom_uid;
	}

	public void setCustom_uid(String custom_uid) {
		this.custom_uid = custom_uid;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	
	
}
