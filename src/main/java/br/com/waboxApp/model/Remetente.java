package br.com.waboxApp.model;

import javax.persistence.Entity;

@Entity
public class Remetente extends Pessoa{

	
	
	private String token;
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}


}
