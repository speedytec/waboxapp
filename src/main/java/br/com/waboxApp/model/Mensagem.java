package br.com.waboxApp.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotBlank;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Entity 
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class Mensagem {
	//
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;
	
	@NotBlank
	@OneToOne
	private Remetente remetente;
	
	public Remetente getRemetente() {
		return remetente;
	}
	public void setRemetente(Remetente remetente) {
		this.remetente = remetente;
	}
	@NotBlank
	@OneToMany
	@JoinTable(name="destinatarios_mensagens")
	private List <Destinatario> to;
	
	
	private String Status;
	
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private LocalDateTime dataHoraEnvio;
	
		
	public List<Destinatario> getTo() {
		return to;
	}
	public void setTo(List<Destinatario> to) {
		this.to = to;
	}
	public LocalDateTime getDataHoraEnvio() {
		return dataHoraEnvio;
	}
	public void setDataHoraEnvio(LocalDateTime dataHoraEnvio) {
		this.dataHoraEnvio = dataHoraEnvio;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	
	
	
	
	
}


