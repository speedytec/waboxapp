package br.com.waboxApp.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity 
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class Pessoa implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Integer id;
		
	private String nome;
	
	private String numero;
	
	private String email;
	
	private String endereco;	
	
	private String cidade;	
	
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	private String identificador1;
	
	private String identificador2;
	
	private String identificador3;
	
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
	public String getIdentificador2() {
		return identificador2;
	}
	public void setIdentificador2(String identificador2) {
		this.identificador2 = identificador2;
	}
	public String getIdentificador3() {
		return identificador3;
	}
	public void setIdentificador3(String identificador3) {
		this.identificador3 = identificador3;
	}
	public String getIdentificador1() {
		return identificador1;
	}
	public void setIdentificador1(String identificador1) {
		this.identificador1 = identificador1;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	

}
