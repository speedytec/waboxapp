package br.com.waboxApp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import br.com.waboxApp.model.Destinatario;
import br.com.waboxApp.model.MensagemTexto;
import br.com.waboxApp.model.Remetente;
import br.com.waboxApp.repository.DestinatarioRepository;
import br.com.waboxApp.rest.WaboxappClient;
import br.com.waboxApp.utils.CSVRead;
import br.com.waboxApp.utils.ExcelRead;

/**
 * by leandro Scarassatti
 */
@Controller
public class HomeController {
	
	@Autowired
	DestinatarioRepository destinatarioRepository;

    @GetMapping("/")
    public String home(){
        return "home";
    }
    
	@GetMapping("/csv")
	public String importCSV() {

		CSVRead read = new CSVRead();
	   	// destinatarios.forEach(p -> {System.out.println(p.toString());});
		destinatarioRepository.saveAll(read.lecsv("C:\\Users\\speed\\eclipse-workspace\\waboxapp\\src\\main\\java\\br\\com\\speedytec\\waboxapp\\utils\\contacts.csv"));
		return "home";
	}
	

	@GetMapping("/xlsx")
	public String importXLSX() {

		ExcelRead excelRead = new ExcelRead();
		
		destinatarioRepository.saveAll(excelRead.le("C:\\Users\\speed\\eclipse-workspace"
			      + "\\waboxapp\\src\\main\\java\\br\\com\\speedytec\\waboxapp\\utils\\ped_2022.xlsx"));
		return "home";
	}
	
	@GetMapping("/waboxapp")
	public String waboxapp() {

		MensagemTexto mensagemTexto = new MensagemTexto();
		Remetente remetente = new Remetente();
		Destinatario destinatario= new Destinatario();
		List <Destinatario> destinatarios = new ArrayList<>();
		
		remetente.setNumero("011920025027");
		remetente.setToken("0dc8f31f4356d243521278b8cd80fa0b5ae79648ae2e1");
		
		destinatario.setNumero("011970958280");
		destinatarios.add(destinatario);
		
		mensagemTexto.setText("TESTE DE ENVIO Waboxapp");
		mensagemTexto.setRemetente(remetente);
		mensagemTexto.setCustom_uid("texto0001");
		mensagemTexto.setTo(destinatarios);
		
		WaboxappClient client = new WaboxappClient();
		client.postTexto(mensagemTexto, MensagemTexto.class);
		return "home";
	}

}

/*
 * 
 * 
		MensagemTexto mensagemTexto = new MensagemTexto();
		Remetente remetente = new Remetente();
		Destinatario destinatario= new Destinatario();
		List <Destinatario> destinatarios = new ArrayList<>();
		
		remetente.setNumero("011920025027");
		remetente.setToken("0dc8f31f4356d243521278b8cd80fa0b5ae79648ae2e1");
		
		destinatario.setNumero("011970958280");
		destinatarios.add(destinatario);
		
		mensagemTexto.setText("TESTE DE ENVIO Waboxapp");
		mensagemTexto.setRemetente(remetente);
		mensagemTexto.setCustom_uid("texto0001");
		mensagemTexto.setTo(destinatarios);
		
		WaboxappClient client = new WaboxappClient();
		client.postTexto(mensagemTexto, MensagemTexto.class);
  */
