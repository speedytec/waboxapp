package br.com.waboxApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WaboxAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(WaboxAppApplication.class, args);
	}
}
