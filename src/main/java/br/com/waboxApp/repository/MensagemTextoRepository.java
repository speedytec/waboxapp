package br.com.waboxApp.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.waboxApp.model.MensagemTexto;


public interface MensagemTextoRepository extends CrudRepository<MensagemTexto, Long> {
	

}
