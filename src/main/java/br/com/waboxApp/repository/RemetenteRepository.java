package br.com.waboxApp.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.waboxApp.model.Remetente;


public interface RemetenteRepository extends CrudRepository<Remetente, Long> {

}
