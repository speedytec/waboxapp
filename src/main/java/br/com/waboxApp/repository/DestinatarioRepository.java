package br.com.waboxApp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.waboxApp.model.Destinatario;

public interface DestinatarioRepository extends CrudRepository<Destinatario, Long> {

	List<Destinatario> findByIdentificador1(String identificador1);
	
}
