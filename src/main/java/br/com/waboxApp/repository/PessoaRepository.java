package br.com.waboxApp.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import br.com.waboxApp.model.Pessoa;


public interface PessoaRepository extends CrudRepository<Pessoa, Long> {
	
		
}
