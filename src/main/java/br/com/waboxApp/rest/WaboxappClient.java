package br.com.waboxApp.rest;



import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import br.com.waboxApp.model.Destinatario;
import br.com.waboxApp.model.MensagemMultimidiaImagem;
import br.com.waboxApp.model.MensagemResposta;
import br.com.waboxApp.model.MensagemTexto;



@Component
public class WaboxappClient {
	String url=null;
	private Logger logger = Logger.getLogger(WaboxappClient.class);
	private Optional<MensagemResposta> myFirstOptional;
	
	public <T>Optional<T> postTexto (MensagemTexto mensagemTexto, Class<T> tClass){
		
		RestTemplate client = new RestTemplate();
		List<Destinatario> destinatarios = mensagemTexto.getTo();
		
		destinatarios.forEach(d ->{
				try {
						Thread.sleep(100);
						url = String.format("https://www.waboxapp.com/api/send/chat?token=%s&uid=%s&to=%s&custom_uid=%s&text=%s",
						mensagemTexto.getRemetente().getToken(),
						mensagemTexto.getRemetente().getNumero(),
						"55"+d.getNumero(),
						String.valueOf(LocalDateTime.now()).replace(".","+"),
						"Bom+dia,+"+d.getNome().replace(" ","+")+"+,"+mensagemTexto.getText().replace(" ","+")			
						);
						System.out.println(url);
						myFirstOptional =(Optional<MensagemResposta>) Optional.of(client.getForObject(url, tClass));
				
				} catch (RestClientException | InterruptedException e) {
						e.printStackTrace();
						myFirstOptional.empty();
				}
				
		
		});
		return (Optional<T>) myFirstOptional;
		
	}
	
	
		public <T>Optional<T> postMultimidia (MensagemMultimidiaImagem mensagemMultimidia, Class<T> tClass){
			
			
			RestTemplate client = new RestTemplate();
			List<Destinatario> destinatarios = mensagemMultimidia.getTo();
			
			//Mensagem padrao do pela API
			/*https://www.waboxapp.com/api/send/media?token=my_token&uid=34666123456&to=34666789123&custom_uid=msg0001&text=Hello+dude*/
			
			destinatarios.forEach(d ->{
					try {
						Thread.sleep(300);
						url = String.format("https://www.waboxapp.com/api/send/media?"
								+ "token=%s&uid=%s&to=%s&custom_uid=%s&url=%s&caption=%s&description=%s&url_thumb=%s",
						mensagemMultimidia.getRemetente().getToken(),
						mensagemMultimidia.getRemetente().getNumero(),
						"55"+d.getNumero(),
						String.valueOf(LocalDateTime.now()).replace(".","+"),
						mensagemMultimidia.getUrl(),
						mensagemMultimidia.getCaption().replace(" ","+"),
						mensagemMultimidia.getDescription().replace(" ","+"),
						mensagemMultimidia.getUrl_thumb().replace(" ","+")
										
						);
						System.out.println(url);
						myFirstOptional =(Optional<MensagemResposta>) Optional.of(client.getForObject(url, tClass));
					} catch (RestClientException | InterruptedException e) {
						e.printStackTrace();
						myFirstOptional.empty();
					}
					
			});
			return (Optional<T>) myFirstOptional;
			
		
	}
	

}