package br.com.waboxApp.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import br.com.waboxApp.model.Destinatario;

public class CSVRead {
	
	public List<Destinatario> destinatarios = new ArrayList<>();

	public List<Destinatario> lecsv(String stringFile) {
		
		try {
			//URL url = new URL(stringFile);
			InputStream is = new FileInputStream(stringFile);
			System.out.println(is.toString());
			BufferedReader in = new BufferedReader(new InputStreamReader(is));
			//InputStreamReader in = getClass().getResourceAsStream(null);
				     // .getResourceAsStream("br/com/speedytec/waboxapp/utils/contacts.csv");
			//URL url = getClass().getResource("contacts.csv");
			
				CSVReader csvReader = new CSVReaderBuilder(in).withSkipLines(1).build();
				String[] nextRecord;
				List<String> myList = new ArrayList<String>();
				Destinatario destinatario ;
			
					while ((nextRecord = csvReader.readNext()) != null) {
						destinatario=new Destinatario();
						int x=1;				
							for (String cell : nextRecord) {						
								if(x==1) {myList.add(0, cell);}
								if (x==2&& !cell.isEmpty())
									myList.set(0, myList.get(0)+" "+cell);	
								if (x==3) {
									myList.set(0, myList.get(0)+" "+cell);
									destinatario.setNome(myList.get(0).toString());
								}						
								if (cell.contains("@")) {
									destinatario.setEmail(cell.toString());					
								}						
								if (x==59) {
									cell = cell.replaceAll("[^0-9 ]", "").trim();
									cell = cell.replaceAll(" ", "").trim();
									destinatario.setNumero(cell.toString());	
									destinatario.setIdentificador1("LEADS_200_MES8");
								}
												
								x++;
								
								
							}
							
							destinatarios.add(destinatario);
							destinatarios.forEach(p -> {System.out.println(p.toString());});
						}
				
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
	return destinatarios;
		
	}

	private CSVReader extracted(URL url) throws FileNotFoundException {
		File file = new File(url.getPath());
		FileReader filereader;
		filereader = new FileReader(file);
		CSVReader csvReader = new CSVReaderBuilder(filereader).withSkipLines(1).build();
		return csvReader;
	}

}
