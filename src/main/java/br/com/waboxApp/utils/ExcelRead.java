package br.com.waboxApp.utils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import br.com.waboxApp.model.Destinatario;



public class ExcelRead {
	// private static String fileName = "C:/teste/teste.xls";
	public List<Destinatario> destinatarios = new ArrayList<>();

	public List<Destinatario> le(String stringFile) {

		try {

			InputStream is = new FileInputStream(stringFile);
			System.out.println(is.toString());
			BufferedReader in = new BufferedReader(new InputStreamReader(is));
			Workbook[] wbs = new Workbook[] {new XSSFWorkbook(is)};
			for (int i = 0; i < wbs.length; i++) {
				Workbook workbook = wbs[i];
				
				CreationHelper createHelper = workbook.getCreationHelper();
				
				Sheet sheetDestinatatios = workbook.getSheetAt(0);

				Iterator<Row> rowIterator = sheetDestinatatios.iterator();
				int count = 0;

				while (rowIterator.hasNext()) {
					count ++;
					Row row = rowIterator.next();
					Iterator<Cell> cellIterator = row.cellIterator();
					String telefone = null;
					String endereco = null;
					Destinatario destinatario = new Destinatario();

					while (cellIterator.hasNext()) {
						Cell cell = cellIterator.next();
						switch (cell.getColumnIndex()) {
						case 0:
							destinatario.setNome(cell.getStringCellValue().trim());
							break;
						case 1:
							endereco = cell.getStringCellValue().trim()+". ";
							break;
						case 2:
							endereco += cell.getStringCellValue().trim()+", ";
							break;
						case 3:
							endereco += cell.getStringCellValue().trim()+" - ";
							break;
						case 4:
							endereco += cell.getStringCellValue().trim()+" - ";
							break;
						case 5:
							destinatario.setEndereco(endereco + cell.getStringCellValue().trim());
							break;
						case 6:
							destinatario.setCidade(cell.getStringCellValue().trim());
							break;
						case 17:
							telefone = cell.getStringCellValue();
							break;
						case 18:
							destinatario.setNumero(telefone + cell.getStringCellValue().trim());
							break;
						case 23:
							destinatario.setIdentificador1("Mailing_MORATO");
							destinatario.setEmail(cell.getStringCellValue().trim());
							break;

						}
					}
					destinatarios.add(destinatario);
					System.out.println(count);
				}
				//destinatarios.forEach(p -> {
				//	System.out.println(p.toString());
			//	});
				is.close();
			}

		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Arquivo Excel não encontrado!");
		}

		if (destinatarios.size() == 0) {
			System.out.println("Nenhum destinatario encontrado!");
		}
	return destinatarios;
	}
}
